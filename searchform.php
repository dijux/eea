<?php
/**
    Search Form Template

**/

?>
<div class="recherche-widget" action="<?php echo home_url( '/' ); ?>" method="get" >
    <form >
        <input type="text" name="s" class="search" placeholder="Recherche une formation..." value="<?php the_search_query(); ?>" >
        <button class="submit-search" type="submit"><i class="fa fa-search fa-1x"></i></button>
    </form>
</div>