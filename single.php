
<?php
/*
Template Name: Page - Right Sidebar
*/
?>

<?php get_header(); ?>

<!-- .main-content -->
<section class="main-content container">
  <div class="row">
    <div class="col-lg-9 col-md-9 col-xs-12">

    <?php get_template_part( 'breadcrumb' ); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php $formationDATA = get_post_meta( get_the_id() ,'_custom_post_options' , true); ?>

    <section class="panel">
      <div class="panel-heading">
        <h1 class="page-title entry-title panel-title" itemprop="headline"><?php the_title(); ?></h1>
      </div>
        
      <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix single-formation'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
        
        <header class="page-head article-header">
          
          
        
        </header> <!-- end article header -->
      
        <section class="page-content entry-content clearfix" itemprop="articleBody">
          <?php the_content(); ?>
      
        </section> <!-- end article section -->
        
        
        <footer>

          <?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","bonestheme") . ':</span> ', ', ', '</p>'); ?>
          
        </footer> <!-- end article footer -->
        
      
      </article> <!-- end article -->

    </section>
                
    <?php endwhile; ?>    
    
    <?php else : ?>
    
    <article id="post-not-found">
        <header>
          <h1><?php _e("Page indésponible", "bonestheme"); ?></h1>
        </header>
        <section class="post_content">
          <p><?php _e("Nous sommnes navrés, Aucune page staisfait votre demande", "bonestheme"); ?></p>
        </section>
        <footer>
        </footer>
    </article>
    
    <?php endif; ?>

  </div> <!-- end #main -->

  <?php get_sidebar(); ?>

</div> <!-- end #content -->

</section> <!-- end .container -->

<?php get_footer(); ?>
