
<?php
/*
Template Name: Page - Right Sidebar
*/
?>

<?php get_header(); ?>

<!-- .main-content -->
<section class="main-content container">
  <div class="row">
    <div class="col-lg-9 col-md-9 col-xs-12">

    <?php get_template_part( 'breadcrumb' ); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php $formationDATA = get_post_meta( get_the_id() ,'_custom_post_options' , true); ?>

    <section class="panel">
      <div class="panel-heading">
        <h1 class="page-title entry-title panel-title" itemprop="headline"><?php the_title(); ?></h1>
      </div>
        
      <div id="single-formation-<?php the_ID(); ?>" class="single-formation" style="border-color: <?php echo $formationDATA['color_formation']; ?> ;">
        <div class="row">
          <div class="col-sm-6">
            <span class="labely"><i class="fa fa-qrcode"></i>  Code Formation : </span>
            <span class="bold-blue"><?php echo $formationDATA['code_formation']; ?></span>
          </div>
          <div class="col-sm-5 pull-right">
            <span class="labely"><i class="fa fa-clock-o"></i>  Durée de Formation : </span>
            <span class="bold-blue"><?php echo $formationDATA['duree_formation']; ?> Jours</span>
          </div>

        </div>
        <div class="row">
          <div class="col-sm-7 title-for-list">
            <h5>Objectifs :</h5>
            <p>
            <?php echo $formationDATA['objectifs_formation']; ?>
            </p>
          </div>
          <div class="col-sm-5">
            <div class="formation-lieu">
              <h5><i class="fa fa-map-marker"></i> Lieu de formation</h5>
              <p>
              <?php echo $formationDATA['lieu_formation']; ?></p>
              <p>
              <?php echo $formationDATA['extra_formation']; ?>
              </p>
            </div>
            <div class="formation-frais">
              <h5><i class="fa fa-money"></i> Frais de Participation:</h5>
              <p><?php echo $formationDATA['cost_formation']; ?></p>
            </div>
          </div>
        </div>
        <div class="row align-center">
          <a href="#preinscription_form" role="button" class="btn btn-lg btn-primary" data-toggle="modal">Se Préinscrire</a>
        </div>
        
      </div>
      <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix single-formation'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
        
        <header class="page-head article-header">
          
          
        
        </header> <!-- end article header -->
      
        <section class="page-content entry-content clearfix" itemprop="articleBody">
          <?php the_content(); ?>
      
        </section> <!-- end article section -->
        
        
        <footer>

          <?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","bonestheme") . ':</span> ', ', ', '</p>'); ?>
          
        </footer> <!-- end article footer -->

      
      </article> <!-- end article -->
      

    </section>
                
    <?php endwhile; ?>    
    
    <?php else : ?>
    
    <article id="post-not-found">
        <header>
          <h1><?php _e("Page indésponible", "bonestheme"); ?></h1>
        </header>
        <section class="post_content">
          <p><?php _e("Nous sommnes navrés, Aucune page staisfait votre demande", "bonestheme"); ?></p>
        </section>
        <footer>
        </footer>
    </article>
    
    <?php endif; ?>

  </div> <!-- end #main -->

  <?php get_sidebar(); ?>

</div> <!-- end #content -->

</section> <!-- end .container -->

<?php get_footer(); ?>
