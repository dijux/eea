<?php
/*
Author : mokhandish.zz.mu


*/



require_once( 'inc/factory/navwalker.php' ); // needed for bootstrap navigation
require_once dirname( __FILE__ ) .'/inc/factory/cs-framework/cs-framework.php';
       
// require get_template_directory() . '/titan-options.php';
define( 'CS_ACTIVE_FRAMEWORK',  true  ); // default true
define( 'CS_ACTIVE_METABOX',    true ); // default true
define( 'CS_ACTIVE_SHORTCODE',  false ); // default true
define( 'CS_ACTIVE_CUSTOMIZE',  false ); // default true

// $titan = TitanFramework::getInstance( 'eaa_theme' );

/* inc/bones.php (functions specific to BREW)
  - navwalker
  - Redux framework
  - Read more > Bootstrap button
  - Bootstrap style pagination
  - Bootstrap style breadcrumbs
*/
require_once( 'inc/factory/brew.php' ); // if you remove this, BREW will break
/*
1. inc/bones.php
    - head cleanup (remove rsd, uri links, junk css, ect)
    - enqueueing scripts & styles
    - theme support functions
    - custom menu output & fallbacks
    - related post function
    - page-navi function
    - removing <p> from around images
    - customizing the post excerpt
    - custom google+ integration
    - adding custom fields to user profiles
*/
require_once( 'inc/factory/bones.php' ); // if you remove this, bones will break
/*
2. inc/custom-post-type.php
    - an example custom post type
    - example custom taxonomy (like categories)
    - example custom taxonomy (like tags)
*/
require_once( 'inc/factory/custom-post-type.php' ); // you can disable this if you like
/*
3. inc/admin.php
    - removing some default WordPress dashboard widgets
    - an example custom dashboard widget
    - adding custom login css
    - changing text in footer of admin
*/
// require_once( 'inc/admin.php' ); // this comes turned off by default
/*
4. inc/translation/translation.php
    - adding support for other languages
*/
// require_once( 'inc/translation/translation.php' ); // this comes turned off by default

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'post-thumb-600', 600, 150, true );
add_image_size( 'post-thumb-300', 300, 100, true );
add_image_size( 'post-featured', 750, 300, true );
add_image_size( 'slide-thumb', 1140, 400, true );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
    register_sidebar(array(
        'id' => 'sidebar1',
        'name' => __( 'Sidebar 1', 'bonestheme' ),
        'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));


// add footer widgets

  register_sidebar(array(
    'id' => 'footer-links',
    'name' => __( 'Footer Widget Links', 'bonestheme' ),
    'description' => __( 'Emplacement des liens en bas de la page (wp_id:footer-links)', 'bonestheme' ),
    'before_widget' => '<div id="%1$s" class="widget widgetFooter links-widget-content %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="links-widget-title">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'footer-second',
    'name' => __( 'Footer Widget Second', 'bonestheme' ),
    'description' => __( '2ème Enmplacement en bas de la page (wp_id:footer-second)', 'bonestheme' ),
    'before_widget' => '<div id="%1$s" class="widget widgetFooter second-widget-content %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="second-widget-title">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'footer-map-location',
    'name' => __( 'Footer widget Map Location', 'bonestheme' ),
    'description' => __( 'Emplacement pour insérer votre localisation (wp_id:footer-map-location)', 'bonestheme' ),
    'before_widget' => '<div id="%1$s map-location" class="widget widgetFooter %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  //   register_sidebar(array(
  //   'id' => 'footer-4',
  //   'name' => __( 'Footer Widget 4', 'bonestheme' ),
  //   'description' => __( 'The fourth footer widget.', 'bonestheme' ),
  //   'before_widget' => '<div id="%1$s" class="widget widgetFooter %2$s">',
  //   'after_widget' => '</div>',
  //   'before_title' => '<h4 class="widgettitle">',
  //   'after_title' => '</h4>',
  // ));

    /*
    to add more sidebars or widgetized areas, just copy
    and edit the above sidebar code. In order to call
    your new sidebar just use the following code:

    Just change the name to whatever your new
    sidebar's id is, for example:

    register_sidebar(array(
        'id' => 'sidebar2',
        'name' => __( 'Sidebar 2', 'bonestheme' ),
        'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));

    To call the sidebar in your template, you can just copy
    the sidebar.php file and rename it to your sidebar's name.
    So using the above example, it would be:
    sidebar-sidebar2.php

    */
} // don't remove this bracket!


require_once dirname( __FILE__ ) .'/inc/factory/widgets.php';


/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class(); ?>>
        <article id="comment-<?php comment_ID(); ?>" class="clearfix comment-container">
            <div class="comment-author vcard">
                <?php
                /*
                    this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
                    echo get_avatar($comment,$size='32',$default='<path_to_url>' );
                */
                ?>
                <?php // custom gravatar call ?>
                <?php
                    // create variable
                    $bgauthemail = get_comment_author_email();
                ?>
                <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=64" class="load-gravatar avatar avatar-48 photo" height="64" width="64" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
                <?php // end custom gravatar call ?>
            </div>
      <div class="comment-content">
        <?php printf(__( '<cite class="fn">%s</cite>', 'bonestheme' ), get_comment_author_link()) ?>
        <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>
        <?php edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ?>
            <?php if ($comment->comment_approved == '0') : ?>
                <div class="alert alert-info">
                    <p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
                </div>
            <?php endif; ?>
            <section class="comment_content clearfix">
                <?php comment_text() ?>
            </section>
            <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div> <!-- END comment-content -->
        </article>
    <?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!

/*************** PINGS LAYOUT **************/

function list_pings( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment; ?>
    <li id="comment-<?php comment_ID(); ?>">
        <span class="pingcontent">
            <?php printf(__('<cite class="fn">%s</cite> <span class="says"></span>'), get_comment_author_link()) ?>
            <?php comment_text(); ?>
        </span>
    </li>
<?php } // end list_pings




/**
    // Helper for the tabs querys
    // jx_menu_formation( $categories, $date )
    // 
**/

function jx_menu_formation( $jxterms ){

    $jx_post_type = 'formations';
    $jx_custom_tax = 'formations_cat';
    $date = date('Y/m/d');
    
    // $jx_formations = array(''); 
    // var_dump($jxterms);

    if (isset($jxterms)) : 
        $tab_id = 1;

        foreach ($jxterms as $cat ) :
            $response = array();

            $today = $date;
            $args = array(
                'post_type' => $jx_post_type,
                'tax_query' => array(
                    array(
                        'taxonomy' => $jx_custom_tax,
                        'field'    => 'id',
                        'terms'    =>  $cat,
                    )),
                'posts_per_page' => 10,
                // 'meta_key' => 'date_formation',
                // 'orderby' => 'meta_value_num',
                // 'order' => 'ASC',
                // 'meta_query' => array(
                //     array(
                //         'key' => 'date_formation',
                //         'compare' => '>=',
                //         'value' => $today
                //     ))
                );
            
            $respFormations = new WP_Query( $args );

            
            
            while($respFormations->have_posts()) : $respFormations->the_post();
                $formationDATA = get_post_meta( get_the_id() ,'_custom_post_options' , true);
                //var_dump($formationDATA);
                $formation_id = get_the_id();
                $categories_name = get_term_by('id', $cat, $jx_custom_tax ); 
                $category_formation[] = array(
                    "formation-type" => $categories_name->name,
                    "formation-details" => array(
                                        'formation-id' => $formation_id, 
                                        'formation-code' => $formationDATA['code_formation'],
                                        'formation-image' => get_attached_media('url'),
                                        'formation-title' => get_the_title(),
                                        'formation-link' => get_the_permalink(),
                                        'formation-color' => $formationDATA['color_formation'],
                                        'formation-date' => $formationDATA['date_formation'],
                                        'formation-duedate' => $formationDATA['duedate_formation'],
                                        'formation-fin' => $formationDATA['datefin_formation'],
                                        'formation-objectifs' => $formationDATA['objectifs_formation'],
                                        'formation-lieu' => $formationDATA['lieu_formation'],
                                        'formation-cost' => $formationDATA['cost_formation'],
                                        'formation-extra' => $formationDATA['extra_formation'],
                                        )
                    );
            endwhile;
            wp_reset_query();
            // var_dump($category_formation);
            $jx_formations[] = $category_formation;
            $category_formation= array(); 
            $tab_id++; 

        endforeach;     
    
    endif;

    return $jx_formations; 
}

/**
    Gallery function ::: Needs galleryID from option theme
**/
function eaa_get_gallery($galleryID, $width, $height){
    if (!isset($width)) {
        $width = 150;
    }
    if (!isset($height)) {
        $height = 150; 
    }

    if (isset($galleryID)) {
        
        $arrayGallery = array(); 
        $temp_data_gallery = cs_get_option( $galleryID );
        $data_gallery = explode(',', $temp_data_gallery); 
        foreach ($data_gallery as $key) {
            $arrayGallery[] = get_bloginfo('url') ."/wp-content/uploads/resize/timthumb.php?src=". wp_get_attachment_url($key) . "&w=". $width ."&h=". $height ."&zc=0"; 
        }

        return $arrayGallery; 
    }

    return; 
}
// function jx_daty_formater($dateInput) {
//     $date = DateTime::createFromFormat('Ymd', $dateInput );
//     // return $date->format('d/m/Y');
// }



// add_action( 'after_setup_theme', 'jx_get_options' );


function unused_meta_boxes() {

    remove_meta_box('commentstatusdiv','formations','normal'); // Comment Status
    remove_meta_box('commentstatusdiv','page','normal'); // Comment Status

    remove_meta_box('postexcerpt','formations','normal'); // Excerpt
    remove_meta_box('postexcerpt','page','normal'); // Excerpt

    remove_meta_box('authordiv','formations','normal'); // Author
    remove_meta_box('authordiv','page','normal'); // Author

    remove_meta_box('commentsdiv','formations','normal'); // Comments
    remove_meta_box('commentsdiv','page','normal'); // Comments

    remove_meta_box('trackbacksdiv','formations','normal'); // Trackbacks
    remove_meta_box('trackbacksdiv','page','normal'); // Trackbacks

    remove_meta_box('postcustom','formations','normal'); // Custom Fields
    remove_meta_box('postcustom','page','normal'); // Custom Fields

    remove_meta_box('slugdiv','formations','normal'); // Slug
    remove_meta_box('slugdiv','page','normal'); // Slug

    remove_meta_box('revisionsdiv','formations','normal'); // Revisions
    remove_meta_box('revisionsdiv','page','normal'); // Revisions

    //remove_meta_box('postimagediv','post','side'); // Featured Image
    remove_meta_box('postimagediv','page','side'); // Featured Image

    remove_meta_box('categorydiv','formations','side'); // Categories

    remove_meta_box('tagsdiv-post_tag','formations','side'); // Tags

    // remove_meta_box('pageparentdiv','page','side'); // Page Parent etc.
}
add_action('admin_head', 'unused_meta_boxes');