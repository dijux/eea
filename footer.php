
    <footer id="footer" class="clearfix ">
      <div id="footer-widgets" class="main-footer">

        <div class="container">

        <div id="footer-wrapper">

          <div class="row">
            <div class="col-sm-6 col-md-4">
              <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-links') ) : ?>
              <?php endif; ?>
            </div> <!-- end widget1 -->

            <div class="col-sm-6 col-md-4">
              <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-second') ) : ?>
              <?php endif; ?>
            </div> <!-- end widget1 -->

            <div class="col-sm-12 col-md-4">
              <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-map-location') ) : ?>
              <?php endif; ?>
            </div> <!-- end widget1 -->


          </div> <!-- end .row -->

        </div> <!-- end #footer-wrapper -->

        </div> <!-- end .container -->
      </div> <!-- end #footer-widgets -->

      <div id="sub-floor" class="footer-credits">
        <div class="container">
          <div class="row">
            
            <div class="col-md-9 col-xs-12 cl-effect-1">
              <?php bones_footer_links_fallback(); ?>
            </div>
            <div class="col-md-3 col-xs-12 copy-rights pull-right">
              <p>&copy; <?php echo date('Y'); ?> Copyrights <?php bloginfo( 'name' ); ?></p>
              <p>Created by <a href="//quicktech.ma">Quicktech</a></p>
            </div>
          </div> <!-- end .row -->
        </div>
      </div>

    </footer> <!-- end footer -->
    
    <section>
      <?php get_template_part('templates/contact/contact-inscription'); ?>
    </section>

    <!-- all js scripts are loaded in library/bones.php -->
    <?php wp_footer(); ?>
    <!-- Hello? Doctor? Name? Continue? Yesterday? Tomorrow?  -->
  </div> 
  <!-- .page-wrapper -->
  </body>

</html> <!-- end page. what a ride! -->
