<aside class="col-lg-3 col-md-3 col-xs-12">


	<?php get_search_form(); ?>

	<?php get_template_part('templates/sidebar/plan-formation'); ?>

	<?php get_template_part('templates/sidebar/contact-widget'); ?>


	<?php if( cs_get_option('media_gallery_feature') ) : ?>

		<?php get_template_part('templates/sidebar/media-widget');  ?>

	<?php endif;  ?>

	<?php get_template_part('templates/sidebar/newsletter-widget') ?>
	

	<?php get_template_part('templates/sidebar/temoignage-widget') ?>

	<?php get_template_part('templates/chat-widget'); ?>
</aside>
