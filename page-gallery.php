<?php
/*
Template Name: Page des Galleries 
*/
?>

<?php get_header(); ?>
<?php 

    // var_dump(cs_get_option('gallery_group_data'));
    $tempGallerie = cs_get_option('gallery_group_data'); 
 ?>

<!-- .main-content -->
<section class="main-content container">
  <div class="row">
    <div class="col-lg-9 col-md-9 col-xs-12">

      <?php get_template_part( 'breadcrumb' ); ?>
      
      <?php if( $tempGallerie !== "" ) :  ?>
      <div class="row">
      <?php foreach($tempGallerie as $item ) : ?>
      
        <article id="post-<?php // the_ID(); ?>" class="col-md-12 col-sm-12 col-xs-12 panel" role="article" itemscope itemtype="http://schema.org/BlogPosting">
          
          <header class="page-head article-header panel-heading">
            <h1 class="page-title entry-title panel-title"  itemprop="headline"><?php echo $item['gallery_title']; ?></h1>
          </header> <!-- end article header -->
        
          <section class="page-content entry-content clearfix" itemprop="articleBody">
            <?php echo $item['gallery_content']; ?>
        
          </section> <!-- end article section -->
          <section class="gallerie-content galleries-sliders">
            <?php  $temmpGalleries = explode(',', $item['galleries_content'] ) ; ?>
            <?php foreach( $temmpGalleries  as $galleryItem ) :  ?>
              <div class="gallery-item">
                <img src='<?php echo get_bloginfo('url') ."/wp-content/uploads/resize/timthumb.php?src=". wp_get_attachment_url($galleryItem) . "&w=848&h=480&zc=1";  ?>'> 
              </div>
            <?php endforeach; ?>
          </section>
          <footer>
    
            <?php // the_tags('<p class="tags"><span class="tags-title">' . __("Tags","bonestheme") . ':</span> ', ', ', '</p>'); ?>
            
          </footer> <!-- end article footer -->
        
        </article> <!-- end article -->
      
        
      <?php endforeach; ?> 
      </div>    
        
          
        
      <?php else : ?>
        
        <article id="post-not-found">
            <header>
              <h1><?php _e("Not Found", "bonestheme"); ?></h1>
            </header>
            <section class="post_content">
              <p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
            </section>
            <footer>
            </footer>
        </article>
        
      <?php endif; ?>
    </div>
    <?php get_sidebar(); ?>

 </div>
</section>
<?php get_footer(); ?>
