<section class="contact-modal">
	<div id="contact_form" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title"><?php if (cs_get_option('contact_popup_switch')) { echo cs_get_option('contact_popup_title');  } else { echo "Envoyer une demande personnalisée";  } ;  ?></h4>
	            </div>

	            <div class="modal-body">
	            	<?php// if(cs_get_option('contact_shortcode') ) :  ?>
						<?php $contact_code = cs_get_option("contact_shortcode") ; ?>
						<?php $forms_popup_content = do_shortcode( $contact_code ); ?>
						<?php echo $forms_popup_content;  ?>
					<?php// else : ?>
						
					<?php // endif; ?>
	            </div>
	            <div class="modal-footer align-center">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
	            </div>
	        </div>
	    </div>
	</div>
	
</section>

