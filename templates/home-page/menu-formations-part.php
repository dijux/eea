<?php
/**
  Theme Name : EEAfric for QuickTech.ma
  Theme author: @dijux 
  Template part for Menu Tabs

**/


    
  $tabs_data = jx_menu_formation(cs_get_option('tabs_menu_cats'));
  
  $tab_id= 1;
  
?>


 <section class="menu-formations">
  <div class="tabs tabs-style-flip">
    <nav>
      <ul>
      <?php foreach ($tabs_data as $tab ) :  ?>
        <li><a href="<?php echo "tab-$tab_id"; ?>" class=""><span>Formation <?php echo $tab[0]['formation-type']; ?></span></a></li>
      
      <?php $tab_id++; ?>

      <?php endforeach;  ?>
        
      </ul>
    </nav>
    <div class="content-wrap">
    <?php $tab_id= 1; ?>
    <?php foreach($tabs_data as $tab) : ?>
      <section id="<?php  echo "tab-$tab_id"; ?>">
        <div class="row">
        <ul class="col-xs-12 col-md-12 cl-effect-2">
      <?php foreach ($tab as $forma ) :  ?>
        
      <li ><a href="<?php echo $forma['formation-details']['formation-link']; ?>" data-hover="<?php echo $forma['formation-details']['formation-title']; ?>" style="background-color: <?php echo $forma['formation-details']['formation-color']; ?> !important;" ><i class="fa fa-chevron-circle-right"></i>  <?php echo $forma['formation-details']['formation-title']; ?></a></li>

      
    <?php endforeach;  ?>
    </ul>
    <?php $tab_id++;  ?>
        </div>
      </section>
      
    <?php endforeach; ?>
    </div><!-- /content -->
  </div><!-- /tabs -->
</section>
<!-- .menu-formations -->
