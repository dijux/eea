<?php if(cs_get_option('references_data')) :  ?>

<section class="nos-references">
	<div class="panel panel">
		<div class="panel-heading">
			<?php if(cs_get_option('refs_panel_title')) : ?>
			<h4 class="panel-title"><i class="fa fa-trophy"></i> <?php echo cs_get_option('refs_panel_title'); ?></h4>
			<?php else : ?>
			<h4 class="panel-title"><i class="fa fa-trophy"></i> Nos Références </h4>
			<?php endif; ?>
		</div>
		
		<div class="references-slider">
		<?php $refs_data = cs_get_option('references_data') ?>
		<?php foreach($refs_data as $refdata) : ?>
			
			<div class="reference-item">
				
				<div class="image">
					<a href='<?php echo $refdata["refs_link"]; ?>'><img src='<?php echo wp_get_attachment_url($refdata["refs_image"]); ?>' alt='<?php echo $refdata["refs_title"]; ?>'></a>
				</div> 
			</div>
		
		<?php endforeach; ?>

			
		</div>
		
		
	</div>
</section>

<?php endif; ?>