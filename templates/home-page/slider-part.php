<?php

$dataslides = cs_get_option('slider_data');


if( !empty($dataslides)) :  ?>
    
            
<div class="container">
    <!-- Slider Part -->
    <!-- Homepage Slider -->
    <div class="homepage-slider">
        <div id="sequence">
            <ul class="sequence-canvas">
          
            <?php foreach ($dataslides as $slide) : ?>

               <?php $image= wp_get_attachment_url($slide['slide_background']);  ?>

                <!-- Slide 1 -->
                <li class="bg4" style="background-image: url(<?php echo get_bloginfo('url');  ?>/wp-content/uploads/resize/timthumb.php?src=<?php echo $image; ?>&w=1140&h=400&zc=1)">
                    <!-- Slide Title -->
                    <h2 class="title"><?php echo $slide['slide_title']; ?></h2>
                    <!-- Slide Text -->
                    <h3 class="subtitle"><?php echo $slide['slide_content']; ?></h3>
                    <!-- Slide Image -->
                    <img class="slide-img" src="<?php echo wp_get_attachment_url($slide['silde_img_right']) ?>" alt="<?php echo $slide['slide_title']; ?>" /> 
                </li>
            <?php endforeach;  ?>

            </ul>

            <div class="sequence-pagination-wrapper">
                <ul class="sequence-pagination">
                    <?php $id=1 ?>
                    <?php foreach ($dataslides as $key) : ?>
                    <li><?php echo $id; ?></li>
                    <?php $id++ ?>
                    <?php endforeach; ?>
                </ul>
                <span class="next"></span>
            </div>
        </div>
    </div>
    <!-- End Homepage Slider -->
</div>
<!-- end .container  -->

<?php endif; ?>