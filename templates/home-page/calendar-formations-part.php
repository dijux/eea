<?php 
		
		$tabs_data = jx_menu_formation(cs_get_option('calendar_cats'));
		$tab_id= 1;
		
	?>

<section class="formations-section">
	<div class="panel">
		<div class="panel-heading">
			<h3 class="panel-title">Calendrier de Formations</h3>
		</div>
		<div class="nounce row">
			<span class="col-sm-8">Cliquer sur les formations pour afficher / masquer le détail de chaque formation</span>
			<span class="col-sm-4 pull-right"><a href=""><i class="fa fa-file-pdf-o"></i> Télécharger le bulletin d'inscription</a></span>
		</div>
		<ul class="list-group calendar-formations">
			<?php if(!empty($tabs_data)) : ?>
			<?php $tab_id = 1;  ?>
			<?php foreach ($tabs_data as $tab ) :  ?>
      		
      		<li class="list-group-item">
				<div class="row toggle first-level-list" id="dropdown-formations-lst-<?php echo "tab-$tab_id"; ?>" data-toggle="formations-lst-<?php echo "tab-$tab_id"; ?>">
					<div class="col-xs-12">
						<h5>Formations <?php echo $tab[0]['formation-type']; ?> <i class="fa fa-chevron-down pull-right"></i></h5>
					</div>
				</div>
				<!-- Second Listing of formations (or even a category) of items -->
				<div id="formations-lst-<?php echo "tab-$tab_id"; ?>">
					<ul class="list-group second-level-list">
						<?php $tabb_id = 1;  ?>
						<?php foreach ($tab as $forma ) :  ?>
							<?php //var_dump($forma) ?>
							<li class="list-group-item" >
								<div class="row toggle" id="dropdow-single-formation-<?php echo $forma['formation-details']['formation-id']; ?>" data-toggle="single-formation-<?php echo $forma['formation-details']['formation-id']; ?>">
									<div class="col-xs-12 formation-title">
										<h5><i class="fa fa-chevron-right pull-left"></i> <?php echo $forma['formation-details']['formation-title']; ?> <span class="formation-date pull-right"><i class="fa fa-calendar fa-1x"></i> <?php echo $forma['formation-details']['formation-date']; ?></span></h5>	
									</div>
								</div>
							</li>
							<div id="single-formation-<?php echo $forma['formation-details']['formation-id']; ?>" class="detail-formation" style="border-color: <?php echo $forma['formation-details']['formation-color']; ?> ;">
								<div class="row">
									<div class="col-sm-4">
										<span>Code Formation : </span>
										<span class="bold-blue"><?php echo $forma['formation-details']['formation-code']; ?></span>
									</div>
									<div class="col-sm-4">
										<span><i class="fa fa-calendar"></i> Date de Formation</span>
										<?php  ?>
										<span class="bold-blue"><?php echo $forma['formation-details']['formation-date']; ?></span>
									</div>
									<div class="col-sm-4">
										<span><i class="fa fa-calendar"></i> Fin d'inscription : </span>
										<span class="bold-blue due-date-registration"><?php echo $forma['formation-details']['formation-duedate']; ?></span>
									</div>

								</div>
								<div class="row">
									<div class="col-sm-7 title-for-list">
										<h5>Objectifs :</h5>
										<p>
										<?php echo $forma['formation-details']['formation-objectifs']; ?>
										</p>
									</div>
									<div class="col-sm-5">
										<div class="formation-lieu">
											<h5><i class="fa fa-map-marker"></i> Lieu de formation</h5>
											<p>
											<?php echo $forma['formation-details']['formation-lieu']; ?></p>
											<p>
											<?php echo $forma['formation-details']['formation-extra']; ?>
											</p>
										</div>
										<div class="formation-frais">
											<h5><i class="fa fa-money"></i> Frais de Participation:</h5>
											<p><?php echo $forma['formation-details']['formation-cost']; ?></p>
										</div>
									</div>
								</div>
								<div class="row align-center">
									<a href="<?php echo $forma['formation-details']['formation-link'] ; ?>"><button class="btn btn-md btn-primary btn-calltoaction">Se Préinscrire</button></a>
								</div>
							</div>
						<?php $tabb_id++; ?>
						<?php endforeach; ?>
						
					</ul>
				</div>
			</li>
      		<?php $tab_id++; ?>

			<?php endforeach;  ?>
		<?php endif; ?>
			

		</ul>
	</div>
</section>