<?php 

	$homePageContent = get_post(cs_get_option('home_page_id')); 
	$title = $homePageContent->post_title;

?>
<section class="" id="#home-page-content">
	<div class="home-page-content">
		<h1 class="home-title"><?php echo $homePageContent->post_title;  ?></h1>
		<div>
			<?php echo $homePageContent->post_content;   ?>
		</div>
		
	</div>
</section>