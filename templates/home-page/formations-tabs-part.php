<?php 
        $args = array(
            'post_type'=> 'slides',
            'showposts' => 10
            );
        $the_query = new WP_Query( $args );
        // if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
        //     $tabsPostID = get_the_ID() ; 
        // endwhile; 


        // checking if the slides exists ( querying the slides  ) 
        $slides = $the_query->get_posts();
        if($the_query->have_posts()):  
?>

<div class="container">
	<!-- Slider Part -->
	<!-- Homepage Slider -->
	<div class="homepage-slider">
		<div id="sequence">
			<ul class="sequence-canvas">
			
			<?php while ($the_query->have_posts()) : ?>

			<?php $the_query->the_post(); ?>

				<!-- Slide 1 -->
				<li class="bg4" style="background-image: url(http://euro-afrik-academy.dev/wordpress/wp-content/uploads/resize/timthumb.php?src=<?php the_field('background_slide') ?>&w=1140&h=400)">
					<!-- Slide Title -->
					<h2 class="title"><?php the_title(); ?></h2>
					<!-- Slide Text -->
					<h3 class="subtitle"><?php the_field('text_left_slide') ?></h3>
					<!-- Slide Image -->
					<img class="slide-img" src="<?php the_field('slide_object_right') ?>" alt="Slide 1" />
				</li>
			<?php endwhile;  ?>
			
		</div>
	</div>
	<!-- End Homepage Slider -->
</div>
<!-- end .container  -->

<?php endif; ?>


<?php 
// still need to add the next and prev buttons


?>