<a href="<?php cs_get_option('plan_foramtion_link') ?>" class="plan-formation-widget row wow flipInY" data-wow-duration="1s" data-wow-delay="1s">
	<div class="box-plan-formation col-xs-12 " >
		<h3 class="plan-formation-widget-title"><?php echo cs_get_option('plan_foramtion_bigtitle') ?></h3>
		<span class="sub-title"><?php echo cs_get_option('plan_foramtion_subtitle') ?></span>
		<div class="pull-right">
			<img src="<?php echo get_template_directory_uri(); ?>/inc/assets/images/teacher.png" alt="Plan de foramtion">
		</div>
	</div>
	
</a>
