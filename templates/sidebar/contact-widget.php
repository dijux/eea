<div class="contact-nous-widget">
  <h3 class="widget-contact-title">Contactez Nous</h3>
  <div class="widget-contact-content">
    <span class="sub-title">Euro Afric Academy</span>
    <p>
      Villa N°256 Lot Sofia El Warda, Targa, Marrakech 40000, Maroc
    </p>
    <span class="tel"><i class="fa fa-phone"></i> Tel: +212 523-9526</span>
    <span class="email"><i class="fa fa-mail-replay"></i> Email: euroafricacademy@gmail.com</span>
    <button href="#contact_form" role="button" class="btn btn-sm btn-primary" data-toggle="modal">Nous envoyer une demande personalisée</button>
  </div>
</div>
