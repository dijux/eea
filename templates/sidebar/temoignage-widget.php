<div class="temoignages-widget">
	<h3 class="widget-temoignages-title">Témoignages</h3>
	<div class="widget-temoignages-content">
		<ul class="temoignages-slider">
			<li class="temoignage-item">
				<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis cupiditate ipsum eos, repudiandae sit sapiente, ipsam quaerat enim natus praesentium maxime corporis, totam possimus illum. Hic esse quam, excepturi suscipit."</p>
				<span class="by-name">Mr.Anthony Hopkins</span>
			</li>
			<li class="temoignage-item">
				<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis cupiditate ipsum eos, repudiandae sit sapiente, ipsam quaerat enim natus praesentium maxime corporis, totam possimus illum. Hic esse quam, excepturi suscipit."</p>
				<span class="by-name">Mr.Charle Dugol</span>
			</li>
			<li class="temoignage-item">
				<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis cupiditate ipsum eos, repudiandae sit sapiente, ipsam quaerat enim natus praesentium maxime corporis, totam possimus illum. Hic esse quam, excepturi suscipit."</p>
				<span class="by-name">Mr.Anthony Hopkins</span>
			</li>
		</ul>

	</div>
</div>