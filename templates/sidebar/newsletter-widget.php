<div class="newsletter-widget">
	<h3 class="widget-newsletter-title"><?php echo cs_get_option('newsletter_widget_title') ?></h3>
	<div class="widget-newsletter-content">
		<p>pour recevoir les dates de nos prochaines formations et suivre notre actualités veiller renseigner votre adresse email</p>
		<input type="text" placeholder="Votre-email@exmaple.com">
		<p><button class="btn btn-sm btn-primary pull-right">Envoyer</button></p>
	</div>
</div>