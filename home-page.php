<?php
/*
Template Name: Page d'Acceuil
*/
?>

<?php get_header(); ?>


    <?php if(cs_get_option('slider_feature')) : ?>
        <?php get_template_part('templates/home-page/slider-part');  ?>
    <?php endif; ?>


<!-- .main-content -->
<section class="main-content container">
  <div class="row">
    <div class="col-lg-9 col-md-9 col-xs-12">

        <?php if( cs_get_option('tabs_menu_feature') ) : ?>
            <?php get_template_part('templates/home-page/menu-formations-part');  ?>
        <?php endif; ?>


        <?php get_template_part('templates/home-page/home-page-part'); ?>


        <?php if(cs_get_option('calendar_feature') == true ) : ?>
            <?php get_template_part('templates/home-page/calendar-formations-part' ); ?>
        <?php endif; ?>


        <?php if(cs_get_option('refs_feature') == true ) : ?>
            <?php get_template_part('templates/home-page/references-part' ); ?>
        <?php endif; ?>

        
        
    
    </div>
    <?php get_sidebar(); ?>

  </div>
</section>
<?php get_footer(); ?>