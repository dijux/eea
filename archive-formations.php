<?php get_header(); ?>

<section class="main-content  container">
	<div class="row">
    	<div class="col-lg-9 col-md-9 col-xs-12 archive-formations">
		<!-- UNCOMMENT FOR BREADCRUMBS -->
		<?php if ( function_exists('custom_breadcrumb') ) { custom_breadcrumb(); } ?> 

		<h1 class="archive-title h2"><i class="fa fa-university "></i> Toutes nos fomations</h1>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix panel' ); ?> role="article">

			<header class="article-header panel-heading">

				<h3 class="h2 panel-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

			</header> <?php // end article header ?>
			<section class="extra-info-article">
				<?php
					printf( __( 'Mis à jour <i class="fa fa-clock-o"></i> <time class="updated" datetime="%1$s" pubdate>%2$s</time> <i class="fa fa-pencil-square"></i> Par  <span class="author"> %3$s</span>.', 'bonestheme' ), get_the_time( 'Y-m-d' ), get_the_time('d-m-Y'), bones_get_the_author_posts_link());
				?>
			</section>

			<section class="entry-content clearfix">
				<div class="image-thumbnail">
					<img src="<?php echo get_bloginfo('url') . '/wp-content/uploads/resize/timthumb.php?src='. wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ) . '&w=770&h=245&zc=1'; ?>" />
				</div>

				<?php the_excerpt(); ?>

			</section> <?php // end article section ?>

			<footer class="article-footer">

			</footer> <?php // end article footer ?>

		</article> <?php // end article ?>

		<?php endwhile; ?>

				<?php if ( function_exists( 'bones_page_navi' ) ) { ?>
						<?php bones_page_navi(); ?>
				<?php } else { ?>
						<nav class="wp-prev-next">
								<ul class="clearfix">
									<li class="prev-link"><?php next_posts_link( __( '&laquo; Older Entries', 'bonestheme' )) ?></li>
									<li class="next-link"><?php previous_posts_link( __( 'Newer Entries &raquo;', 'bonestheme' )) ?></li>
								</ul>
						</nav>
				<?php } ?>

		<?php else : ?>

				<article id="post-not-found" class="hentry clearfix">
					<header class="article-header">
						<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
					</header>
					<section class="entry-content">
						<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
					</section>
					<footer class="article-footer">
							<p><?php _e( 'This is the error message in the custom posty type archive template.', 'bonestheme' ); ?></p>
					</footer>
				</article>

		<?php endif; ?>

	</div> <?php // end #main ?>

	<?php get_sidebar(); ?>

</div> <?php // end #content ?>

</section> <?php // end ./container ?> 

<?php get_footer(); ?>
