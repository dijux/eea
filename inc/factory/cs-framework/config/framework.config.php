<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings      = array(
  'menu_title' => 'EAA Look Options',
  'menu_type'  => 'add_menu_page',
  'menu_slug'  => 'eaa-look',
  'ajax_save'  => false,
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();

// ------------------------------
// Homepage Options Part       -
// ------------------------------
// $options[] = array(
//   'name'   => 'seperator_1',
//   'title'  => 'HomePage Options',
//   'icon'   => 'fa fa-home'
// );

$options[] =  array(
      'name'      => 'logo_options',
      'title'     => 'Logo Options',
      'icon'      => 'fa fa-gears',

      // begin: fields
      'fields'    => array(

        // begin: a field
        array(
          'id'    => 'logo_text',
          'type'  => 'text',
          'title' => 'Logo text',
        ),
        // end: a field

        array(
          'id'    => 'logo_image',
          'type'  => 'image',
          'title' => 'logo du site',
        ),

        array(
          'id'    => 'logo_banner',
          'type'  => 'image',
          'title' => 'Banner du site',
        ),

        array(
          'id'    => 'breadcrumb_on',
          'type'  => 'switcher',
          'title' => 'Activer ou désactiver, Localisateur au pages / Navigateur ou vous etes sur les pages ',
          'default' => 'false'
        ),

        array(
          'id'            => 'home_page_id',
          'type'          => 'select',
          'title'         => 'Selectionner la page que son contenu apparaît à la page d\'Accueil. ',
          'options'       => 'pages',
        ),
        

      ), // end: fields

    );// end: text options

// ------------------------------
// a option section with tabs   -
// ------------------------------
$options[]   = array(
  'name'     => 'options',
  'title'    => 'General',
  'icon'     => 'fa fa-plus-circle',
  'sections' => array(

    // -----------------------------
    // begin: General Options      -
    // -----------------------------
    array(
      'name'      => 'logo_options',
      'title'     => 'Logo Options',
      'icon'      => 'fa fa-gears',

      // begin: fields
      'fields'    => array(

        // begin: a field
        array(
          'id'    => 'logo_text',
          'type'  => 'text',
          'title' => 'Logo text',
        ),
        // end: a field

        array(
          'id'    => 'logo_image',
          'type'  => 'image',
          'title' => 'logo du site',
        ),

        array(
          'id'    => 'logo_banner',
          'type'  => 'image',
          'title' => 'Banner du site',
        ),

        array(
          'id'    => 'breadcrumb_on',
          'type'  => 'switcher',
          'title' => 'Activer ou désactiver, Localisateur au pages / Navigateur ou vous etes sur les pages ',
          'default' => 'false'
        ),

        array(
          'id'            => 'home_page_id',
          'type'          => 'select',
          'title'         => 'Selectionner la page que son contenu apparaît à la page d\'Accueil. ',
          'options'       => 'pages',
        ),
        

      ), // end: fields

    ), // end: text options


    // -----------------------------
    // begin: SideBar Options      -
    // -----------------------------
    array(
      'name'      => 'sidebar_options',
      'title'     => 'Sidebar Options',
      'icon'      => 'fa fa-gears',

      // begin: fields
      'fields'    => array(

        array(
          'type'    => 'heading',
          'content' => 'Première Widget Side Bar'
        ),
        
        array(
          'id'    => 'plan_foramtion_bigtitle',
          'type'  => 'text',
          'title' => 'Grand titre',
        ),
        array(
          'id'    => 'plan_foramtion_subtitle',
          'type'  => 'text',
          'title' => 'Sous titre',
        ),
        array(
          'id'    => 'plan_foramtion_link',
          'type'  => 'text',
          'title' => 'Lien URL : ',
        ),

        array(
          'id'      => 'plan_foramtion_color',
          'type'    => 'color_picker',
          'title'   => 'Couleur du fond',
          'default' => '#34495e',
        ),

        array(
          'type'    => 'heading',
          'content' => 'Contact Side Bar Widget'
        ),

        array(
          'id'    => 'switch_contact_widget',
          'type'  => 'switcher',
          'title' => 'Activation Contact Widget',
          'default'=> 'false',
        ),

        array(
          'id'    => 'contact_widget_title',
          'type'  => 'text',
          'title' => 'Titre de la Widget',
        ),
        array(
          'id'    => 'contact_widget_content',
          'type'        => 'wysiwyg',
          'title'       => 'Contenu de la Widget',
          'settings' => array(
              'textarea_rows' => 5,
              'media_buttons' => false,
            )
        ),
        array(
          'type'    => 'heading',
          'content' => 'Demande personnalisé Popup'
        ),
        array(
          'id'    => 'contact_popup_switch',
          'type'  => 'switcher',
          'title' => 'Activation Popup',
          'default'=> 'true',
        ),
        array(
          'id'    => 'contact_popup_title',
          'type'  => 'text',
          'title' => 'Titre de la popup',
        ),
        array(
          'id'    => 'contact_shortcode',
          'type'        => 'wysiwyg',
          'settings' => array(
              'textarea_rows' => 1,
              'media_buttons' => false,
            ),
          'title' => 'Shortcode du formulaire de la demande personnalisé; Utilisez le plugin Contact Form 7 pour créer votre formulaire! ',
        ),

        array(
          'type'    => 'heading',
          'content' => 'Pré-Inscription Popup'
        ),
        
        array(
          'id'    => 'preinscription_shortcode',
          'type'        => 'wysiwyg',
          'settings' => array(
              'textarea_rows' => 1,
              'media_buttons' => false,
            ),
          'title' => 'Shortcode du formulaire de pré-inscription; Utilisez le plugin Contact Form 7 pour créer votre formulaire! ',
        ),

        // Activate the Slide post type
        // array(
        //   'id'    => 'switch_slider_post',
        //   'type'  => 'switcher',
        //   'title' => 'Activation du Slider',
        //   'default'=> 'false',
        //   'desc' => 'Après l\'activation du Slider, Vous pouvez ajouter des slides et puis activer l\'affichage sur la page d\'accueil',
        // ),
        
        

        
      ), // end: fields
    ),
    // -----------------------------
    // begin: Menu des Formation Options      -
    // -----------------------------
    array(
      'name'      => 'tabs_menu_options',
      'title'     => 'Menu des Formations',
      'icon'      => 'fa fa-bars',

      // begin: fields
      'fields'    => array(

        // Activate the Slide post type
        array(
          'id'    => 'tabs_menu_post',
          'type'  => 'switcher',
          'title' => 'Activation du Menu des foramtions',
          'defaul'=> 'false',
          'desc' => 'Après l\'activation du Menu formations, Vous pouvez ajouter des formations et puis activer l\'affichage sur la page d\'accueil',
        ),
        
        array(
          'id'        => 'tabs_menu_feature',
          'type'      => 'switcher',
          'title'     => 'Afficher le Menu des Formations',
          'default' => 'false',
          'dependency'   => array( 'tabs_menu_post', '==', 'true' ),
        ),

        array(
          'id'             => 'tabs_menu_cats',
          'type'           => 'select',
          'title'          => 'Selectioner Une catégorie à afficher dans le menu',
          'options'        => 'categories',
          'attributes'     => array(
            'multiple'     => 'only-key',
            'placehoder'   => 'Selectionner une ou multiple categories de foramtions',
            'style'            => 'width: 300px;'
          ),
          'class'              => 'chosen',
          'query_args'     => array(
            'type'         => 'formations',
            'taxonomy'     => 'formations_cat',
            'orderby'      => 'post_date',
            'order'        => 'DESC',
          ),

          'dependency'   => array( 'tabs_menu_post', '==', 'true' ),
        ),

      ), // end: fields

    ), // end: Menu Tabs options
    // -----------------------------
    // begin: Calendar Formation Options      -
    // -----------------------------
    array(
      'name'      => 'calendar_options',
      'title'     => 'Calendrier des Formations',
      'icon'      => 'fa fa-calendar-o',

      // begin: fields
      'fields'    => array(

        array(
          'id'           => 'calendar_notice',
          'type'         => 'notice',
          'class'        => 'danger',
          'content'      => 'Activer le Menu des Formations pour avoir access !',
          'dependency'   => array( 'tabs_menu_post', '==', 'false' ),
        ),
        
        array(
          'id'        => 'calendar_feature',
          'type'      => 'switcher',
          'title'     => 'Afficher le Calendrier des Formations',
          'default' => 'false',
          'dependency'   => array( 'tabs_menu_post', '==', 'true' ),
        ),

        array(
          'id'             => 'calendar_cats',
          'type'           => 'select',
          'title'          => 'Selectioner Une catégorie de Calendrier',
          'options'        => 'categories',
          'attributes'     => array(
            'multiple'     => 'only-key',
            'placehoder'   => 'Selectionner une ou multiple categories de slide',
            'style'            => 'width: 300px;'
          ),
          'class'              => 'chosen',
          'query_args'     => array(
            'type'         => 'formations',
            'taxonomy'     => 'formations_cat',
            'orderby'      => 'post_date',
            'order'        => 'DESC',
          ),

          'dependency'   => array( 'tabs_menu_post', '==', 'true' ),
        ),

      ), // end: fields

    ), // end: Calendar options
    
    // -----------------------------
    // begin: Group Slider Options      -
    // -----------------------------  
    array(
      'name'      => 'slider_content',
      'title'     => 'Slider Content',
      'icon'      => 'fa fa-circle-o',
      'fields'    => array(
        array(
          'type'    => 'heading',
          'content' => 'Slider Manager'
        ),

        array(
          'id'        => 'slider_feature',
          'type'      => 'switcher',
          'title'     => 'Afficher le Slider',
          'default' => 'false',
          'dependency'   => array( 'switch_slider_post', '==', 'true' ),
        ),

       
        array(
          'id'              => 'slider_data',
          'type'            => 'group',
          'title'           => 'Modifier le contenu de vos Slides comme vous désirez',
          'button_title'    => 'Ajouter Slide',
          'accordion_title' => 'Ajouter des Donnes de Slide',
          'fields'          => array(

            array(
              'id'          => 'slide_title',
              'type'        => 'text',
              'title'       => 'Titre de la Slide',
            ),

            array(
              'id'          => 'slide_content',
              'type'        => 'textarea',
              'title'       => 'Contenu de la Slide',
            ),
            array(
              'id'          => 'slide_background',
              'type'        => 'image',
              'title'       => 'Image de fond de la Slide',
            ),
            array(
              'id'          => 'silde_img_right',
              'type'        => 'image',
              'title'       => 'Image en transition a droite',
            ),

          ),
          
          'default' => array(
            array(
              'slide_title'     => 'Donner un titre a la Slide',
              'slide_content'   => 'Mettez du contenu de la Slide',
            )
          )
        ),

        
       

      ),
    ),

    // -----------------------------
    // begin: References Slider Options      -
    // -----------------------------  
    array(
      'name'      => 'refs_content',
      'title'     => 'Nos Références Content',
      'icon'      => 'fa fa-trophy',
      'fields'    => array(

        array(

          'id' => 'refs_panel_title',
          'title' => 'Titre de panneau de nos Références',
          'type' => 'text', 
        ),

        array(
          'id'    => 'refs_feature',
          'type'  => 'switcher',
          'title' => 'Active ou désactive, Le Slider des Références.',
          'default' => 'false'
        ),
       
        array(
          'id'              => 'references_data',
          'type'            => 'group',
          'title'           => 'Modifier le contenu de vos Références comme vous désirez',
          'button_title'    => 'Ajouter une Référence',
          'accordion_title' => 'Nos Références',
          'fields'          => array(

            array(
              'id'          => 'refs_title',
              'type'        => 'text',
              'title'       => 'Titre de la Référence',
            ),

            array(
              'id'          => 'refs_content',
              'type'        => 'wysiwyg',
              'title'       => 'Contenu de la Référence',
              'settings' => array(
                  'textarea_rows' => 5,
                  'media_buttons' => false,
                )
            ),
            array(
              'id'          => 'refs_link',
              'type'        => 'text',
              'title'       => 'Lien vers la Référence',
            ),
            array(
              'id'          => 'refs_image',
              'type'        => 'image',
              'title'       => 'Image a mettre dans le slider des Références',
            ),

          ),
          
          'default' => array(
            array(
              'slide_title'     => 'Donner un titre pour à la Référence',
              'slide_content'   => 'Mettez du contenu de la Référence',
            )
          )
        ),

      ),
    ), // end: group options
    

    // -----------------------------
    // begin: Temoignages Slider Options      -
    // -----------------------------  
    array(
      'name'      => 'temoignages_content',
      'title'     => 'Nos Témoignages',
      'icon'      => 'fa fa-trophy',
      'fields'    => array(

        array(

          'id' => 'temoi_panel_title',
          'title' => 'Titre de panneau de nos Témoignages',
          'type' => 'text', 
        ),

        array(
          'id'    => 'temoi_feature',
          'type'  => 'switcher',
          'title' => 'Active ou désactive, Le Slider des Témoignages.',
          'default' => 'false'
        ),
       
        array(
          'id'              => 'temoignages_data',
          'type'            => 'group',
          'title'           => 'Modifier le contenu de vos Témoignages comme vous désirez',
          'button_title'    => 'Ajouter un témoignage',
          'accordion_title' => 'Nos Témoignages',
          'fields'          => array(

            array(
              'id'          => 'temoi_title',
              'type'        => 'text',
              'title'       => 'Titre de le témoignage',
            ),

            array(
              'id'          => 'temoi_content',
              'type'        => 'wysiwyg',
              'title'       => 'Contenu de le témoignage',
              'settings' => array(
                  'textarea_rows' => 5,
                  'media_buttons' => false,
                )
            ),
            array(
              'id'          => 'temoi_link',
              'type'        => 'text',
              'title'       => 'Lien vers l témoignage',
            ),
            array(
              'id'          => 'temoi_image',
              'type'        => 'image',
              'title'       => 'Image a mettre dans le slider des Témoignages',
            ),

          ),
          
          'default' => array(
            array(
              'slide_title'     => 'Donner un titre pour à le témoignage',
              'slide_content'   => 'Mettez du contenu de le témoignage',
            )
          )
        ),

      ),
    ), // end: group options


),
  
);

// ------------------------------
// Slider Options Part 
// ------------------------------

$options[]   = array(
  'name'     => 'social_and_sharing',
  'title'    => 'Social & Sharing ',
  'icon'     => 'fa fa-share',
  'fields'   => array(

    array(
      'type'    => 'heading',
      'content' => 'Social and Sharing Setting'
    ),
    
    array(
      'name' => 'social_feature',
      'type' => 'switcher',
      'title' => 'Active ou désactive les options de partage',
      'default' => 'false', 
    ),
    array(
      'name' => 'social_facebook',
      'type' => 'text',
      'title' => '<i class="fa fa-facebook fa-1x"></i> La page facebook ',
      'placeholder' => 'http://socialProvider.com/yourAwesomePage', 
    ), 
    array(
      'name' => 'social_twitter',
      'type' => 'text',
      'title' => '<i class="fa fa-twitter fa-1x"></i> La page Twitter ',
      'placeholder' => 'http://socialProvider.com/yourAwesomePage', 
    ), 
    array(
      'name' => 'social_linkedin',
      'type' => 'text',
      'title' => '<i class="fa fa-linkedin fa-1x"></i> La page Linkedin ',
      'placeholder' => 'http://socialProvider.com/yourAwesomePage', 
    ), 

  )
);



$options[]   = array(
  'name'     => 'media_gallery',
  'title'    => 'Media Gallery',
  'icon'     => 'fa fa-photo',
  'fields'   => array(

    array(
      'type'    => 'heading',
      'content' => 'Simple Media Gallery Manager'
    ),
    
    array(
      'id' => 'media_gallery_feature',
      'type' => 'switcher',
      'title' => 'Active ou désactive la Widget Media.',
      'help' => 'Qui se trouve sur la barre latérale',
      'default' => 'false', 
    ),
    
    array(

      'id' => 'media_panel_title',
      'title' => 'Titre de la Widget Media Gallery',
      'type' => 'text', 
    ),

    array(
      'id'          => 'gallery_media',
      'type'        => 'gallery',
      'title'       => 'Gallery affiché sur la Widget Media Gallery',
      'desc'        => 'Ajouter plusieurs Images',
      'add_title'   => 'Ajouter une/des Image(s)',
      'edit_title'  => 'Editer une/des Image(s)',
      'clear_title' => 'Suprimer une/des Image(s)',
    ),

    array(
      'id'              => 'gallery_group_data',
      'type'            => 'group',
      'title'           => 'Modifier de Votre Gallerie',
      'button_title'    => 'Ajouter une Gallerie',
      'accordion_title' => 'Les Galleries',
      'fields'          => array(

        array(
          'id'          => 'gallery_title',
          'type'        => 'text',
          'title'       => 'Titre de la gallerie',
        ),

        array(
          'id'          => 'gallery_content',
          'type'        => 'wysiwyg',
          'title'       => 'Contenu de le témoignage',
          'settings' => array(
              'textarea_rows' => 5,
              'media_buttons' => false,
            )
        ),
        array(
          'id'          => 'gallery_link',
          'type'        => 'text',
          'title'       => 'Lien vers l témoignage',
        ),
        array(
          'id'          => 'galleries_content',
          'type'        => 'gallery',
          'title'       => 'Gallery affiché sur la Page des Galleries',
          'desc'        => 'Ajouter plusieurs Images',
          'add_title'   => 'Ajouter une/des Image(s)',
          'edit_title'  => 'Editer une/des Image(s)',
          'clear_title' => 'Suprimer une/des Image(s)',
        ),

      ),
      
      'default' => array(
        array(
          'slide_title'     => 'Donner un titre pour à la Gallerie',
          'slide_content'   => 'Mettez du contenu de la Gallerie',
        )
      )
    ),


  )
);


// // ------------------------------
// // a seperator                  -
// // ------------------------------
// $options[] = array(
//   'name'   => 'seperator_3',
//   'title'  => 'Others',
//   'icon'   => 'fa fa-gift'
// );


// ------------------------------
// license                      -
// ------------------------------
$options[]   = array(
  'name'     => 'license_section',
  'title'    => 'License',
  'icon'     => 'fa fa-info-circle',
  'fields'   => array(

    array(
      'type'    => 'heading',
      'content' => 'Theme Développer par QuickTech.ma pour la Euro Afric Academy.'
    ),
    array(
      'type'    => 'content',
      'content' => 'Les droits d\'utilisation de theme sont garantis à Euro Afric Academy et non autre partie. Pour toute informations contactez nous à <a hreg="http://quicktech.ma/contact" >QuickTech.ma</a>',
    ),

  )
);

CSFramework::instance( $settings, $options );
