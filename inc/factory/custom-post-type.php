<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the custom type
function formations_custom_post() { 
	// creating (registering) the custom type 
	register_post_type( 'formations', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Formation', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Formation', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'Toutes les Formations', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Ajouter Formation', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Ajouter une nouvelle formation', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Modifier', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Modifier la Formation', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'Nouvelle Formation', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'Voir la Formation', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Rechercher une formation', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Aucune Formation n\'a été trouvé !', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Acune formation torouvé dans la corbeille', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Ceci est un example pour une nouvelle formation...', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_template_directory_uri() . '/inc/assets/images/university.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'formations', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'formations', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky', 'page-attributes')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type( 'category', 'formations' );
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type( 'post_tag', 'formations' );
	
} 

// if ( !empty(cs_get_option('tabs_menu_post')) && cs_get_option('tabs_menu_post')) {
	# code...
	// adding the function to the Wordpress init
	add_action( 'init', 'formations_custom_post');
	
// }


// now let's add custom categories (these act like categories)
register_taxonomy( 'formations_cat', 
	array('formations'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */             
		'labels' => array(
			'name' => __( 'Categories des Formations', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Categorie de formation', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Chercher des categories de formations', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'Toutes les categories de formations', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Categorie parente', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'categorie parente de formation:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Editez Categorie de formation', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Mettre a jour Categorie de formation', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Ajouter categorie de formation', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'Nouvelle Categorie de formation', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'categorie-formation' ),
	)
);

// // now let's add custom tags (these act like categories)
//    register_taxonomy( 'custom_tag', 
//    	array('custom_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
//    	array('hierarchical' => false,    /* if this is false, it acts like tags */                
//    		'labels' => array(
//    			'name' => __( 'Custom Tags', 'bonestheme' ), /* name of the custom taxonomy */
//    			'singular_name' => __( 'Custom Tag', 'bonestheme' ), /* single taxonomy name */
//    			'search_items' =>  __( 'Search Custom Tags', 'bonestheme' ), /* search title for taxomony */
//    			'all_items' => __( 'All Custom Tags', 'bonestheme' ), /* all title for taxonomies */
//    			'parent_item' => __( 'Parent Custom Tag', 'bonestheme' ), /* parent title for taxonomy */
//    			'parent_item_colon' => __( 'Parent Custom Tag:', 'bonestheme' ), /* parent taxonomy title */
//    			'edit_item' => __( 'Edit Custom Tag', 'bonestheme' ), /* edit custom taxonomy title */
//    			'update_item' => __( 'Update Custom Tag', 'bonestheme' ), /* update title for taxonomy */
//    			'add_new_item' => __( 'Add New Custom Tag', 'bonestheme' ), /* add new title for taxonomy */
//    			'new_item_name' => __( 'New Custom Tag Name', 'bonestheme' ) /* name title for taxonomy */
//    		),
//    		'show_admin_column' => true,
//    		'show_ui' => true,
//    		'query_var' => true,
//    	)
//    ); 

function slider_post_type() { 
	
	register_post_type( 'slides', 

		array( 'labels' => array(
			'name' => __( 'Slide', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Slide', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'Toutes les slides', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Ajouter slide', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Ajouter une nouvelle slide', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Modifier', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Modifier la Slide', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'Nouvelle Slide', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'Voir la Slide', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Rechercher une Slide', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Aucune Slide n\'a été trouvé !', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Acune formation torouvé dans la corbeille', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Ceci est un example pour une nouvelle formation...', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_template_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> false, /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type( 'category', 'formations' );
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type( 'post_tag', 'formations' );
	
} 


// if (!empty(cs_get_option('switch_slider_post')) && cs_get_option('switch_slider_post') ) {
	
	// activate the refrecnes_post_type
	// add_action( 'init', 'slider_post_type');
	
// }	
// let's create the function for the custom type
function refrences_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'refrences', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Nos Référence', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Référence', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'Toutes les Référence', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Ajouter Référence', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Ajouter une nouvelle Référence', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Modifier', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Modifier la Référence', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'Nouvelle Référence', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'Voir la Référence', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Rechercher une Référence', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Aucune Référence n\'a été trouvé !', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Acune Référence torouvé dans la corbeille', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Ceci est un example pour une nouvelle Référence...', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_template_directory_uri() . '/inc/assets/images/formations-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> false, /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type( 'category', 'formations' );
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type( 'post_tag', 'formations' );
	
}


// if (!empty(cs_get_option('switch_refs_post')) && cs_get_option('switch_refs_post') ) {
	
	// activate the refrecnes_post_type
	// add_action( 'init', 'refrences_post_type');
	
// }	

?>
