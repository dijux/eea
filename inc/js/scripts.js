/*
Bones Scripts File
Author: Eddie Machado

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/

// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        }
        return this;
    }
}

// as the page loads, call these scripts
jQuery(document).ready(function($) {

    /*
    Responsive jQuery is a tricky thing.
    There's a bunch of different ways to handle
    it, so be sure to research and find the one
    that works for you best.
    */
    
    /* getting viewport width */
    var responsive_viewport = $(window).width();
    
    /* if is below 481px */
    if (responsive_viewport < 481) {
    
    } /* end smallest screen */
    
    /* if is larger than 481px */
    if (responsive_viewport > 481) {
        
    } /* end larger than 481px */
    
    /* if is above or equal to 768px */
    if (responsive_viewport >= 768) {
    
        /* load gravatars */
        $('.comment img[data-gravatar]').each(function(){
            $(this).attr('src',$(this).attr('data-gravatar'));
        });
        
    }
    
    /* off the bat large screen actions */
    if (responsive_viewport > 1030) {
        
    }
    
	
	// add all your scripts here

    
    //Homepage Slider
    var options = {
        nextButton: true,
        prevButton: true,
        pagination: true,
        animateStartingFrameIn: true,
        autoPlay: true,
        autoPlayDelay: 5000,
        preloader: true,
    };

    var mySequence = $("#sequence").sequence(options).data("sequence");


    //Products slider
    var refsSlider = $('.references-slider').bxSlider({
        slideWidth: $('.references-slider ').outerWidth()-20, //Gets slide width
        responsive: true,
        minSlides: 4,
        maxSlides: 4,
        slideMargin: 20,
        auto: true,
        autoHover: true,
        speed: 800,
        pager: false,
        controls: true,
        nextText: '<i class="fa fa-chevron-right"></i>',
        prevText: '<i class="fa fa-chevron-left"></i>',
    });

    var temoigngesSlider = $('.temoignages-slider').bxSlider({
        slideWidth: $('.temoignages-slider .temoignage-item').outerWidth()-20,
        responsive: true,
        minSlides: 1,
        maxSlides:1,
        auto: true,
        autoHover: true,
        speed:800,
        nextText: '<i class="fa fa-chevron-right"></i>',
        prevText: '<i class="fa fa-chevron-left"></i>',
        controls: true,
        pager: false
    });
    var GalleriesSlider = $('.galleries-sliders').bxSlider({
        slideWidth: $('.galleries-sliders .gallery-item').outerWidth()-20,
        responsive: true,
        minSlides: 1,
        maxSlides:1,
        auto: true,
        autoHover: true,
        speed:1000,
        nextText: '<i class="fa fa-chevron-right"></i>',
        prevText: '<i class="fa fa-chevron-left"></i>',
        controls: true,
        pager: false
    });
    

    $('.detail-formation').hide();
    $('.toggle').click(function() {
        $input = $( this );
        console.log(); 
        $target = $('#'+$input.attr('data-toggle'));
        $target.slideToggle('fast');
    });

    // //Make Videos Responsive
    // $(".video-wrapper").fitVids();

    // //Initialize tooltips
    // $('.show-tooltip').tooltip();

    $('ul.sf-menu').superfish({ pathClass:'current'});
	
    new WOW().init();

    // Sticky Menu 

    // var sticky = document.querySelector('.main-menu');
    // var origOffsetY = sticky.offsetTop;



    // function onScroll(e) {
    //     window.scrollY >= origOffsetY ? sticky.classList.add('navbar-fixed-top') :
    //         sticky.classList.remove('navbar-fixed-top');
    // }

    // document.addEventListener('scroll', onScroll);

    // Change title type, overlay closing speed
    $(".fancybox-effects-a").fancybox({
        helpers: {
            title : {
                type : 'outside'
            },
            overlay : {
                speedOut : 0
            }
        }
    });

}); /* end of as page load scripts */


(function() {

    [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
        new CBPFWTabs( el );
    });

})();

/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
    var doc = w.document;
    if( !doc.querySelector ){ return; }
    var meta = doc.querySelector( "meta[name=viewport]" ),
        initialContent = meta && meta.getAttribute( "content" ),
        disabledZoom = initialContent + ",maximum-scale=1",
        enabledZoom = initialContent + ",maximum-scale=10",
        enabled = true,
		x, y, z, aig;
    if( !meta ){ return; }
    function restoreZoom(){
        meta.setAttribute( "content", enabledZoom );
        enabled = true; }
    function disableZoom(){
        meta.setAttribute( "content", disabledZoom );
        enabled = false; }
    function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
        if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );
})( this );