<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">

	<?php // Google Chrome Frame for IE ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php if (is_front_page()) { bloginfo('name'); } else { wp_title(''); } ?></title>

	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png?v=2">
	<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<![endif]-->
	<?php // or, set /favicon.ico for IE10 win ?>
	<meta name="msapplication-TileColor" content="#f01d4f">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php // wordpress head functions ?>
	<?php wp_head(); ?>
	<?php // end of wordpress head ?>
  
	<?php // drop Google Analytics Here ?>

	<?php // end analytics ?>
  <script src="<?php echo get_template_directory_uri(); ?>/inc/js/wow.min.js"></script>
</head>

<body <?php body_class(); ?>>
  <div class="page-wrapper">
    <header>
      
      <!-- Top Bar Notifications and socials buttons -->
      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><i class="fa fa-cart fa-2x"></i></a>
          </div>
          <div class="navbar-collapse collapse top-navbar">
            <ul class="row">
              <li class="logo-icon col-xs-2 col-sm-2 pull-left ">
                <a href="<?php  echo home_url( '/' ); ?>">
                  <img src="<?php echo wp_get_attachment_url(cs_get_option('logo_image') );  ?>" alt="<?php echo cs_get_option('logo_text');  ?>">
                  <span><?php echo cs_get_option('logo_text') ;  ?></span>
                </a>
              </li>
              <li class="social-facebook col-sm-1 col-xs-push-4">
                <a href="<?php echo cs_get_option('facebook_page_id'); ?>"><i class="fa fa-2x fa-facebook"></i></a>
              </li>
              <li class="social-twitter col-sm-1 col-xs-push-4">
                <a href="<?php echo cs_get_option('twitter_page_id'); ?>"><i class="fa fa-2x fa-twitter"></i></a>
              </li>
              <li class="social-linkedin col-sm-1 col-xs-push-4">
                <a href="<?php echo cs_get_option('linkedin_page_id'); ?>"><i class="fa fa-2x fa-linkedin"></i></a>
              </li>
              <li class="col-xs-12 col-sm-4 pull-right">
                <form action="<?php echo home_url( '/' ); ?>" class="" role="search" method="get">
                  <div class="form-group">
                    <input name="s" type="text" class="form-control recherche" placeholder="Recherche de formation par mot clé" value="<?php the_search_query(); ?>">
                  </div>
                  <button type="submit" class="submit-search"><i class="fa fa-search fa-1x"></i></button>
                </form>
              </li>
                        
            </ul>
          </div>
        </div>
      </div>
    
      <section class="container wow flipInX">
          <div class="row visible-lg">
            <div class="col-xs-12">
              <img src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/resize/timthumb.php?src=<?php echo wp_get_attachment_url(cs_get_option('logo_banner') );  ?>&w=1140&h=120&zc=0" alt="">
            </div>
          </div>
          <div class="row hidden-lg">
              <div class="col-md-12  ">
                <?php bones_mobile_nav(); ?>
              </div>
          </div>
      </section>

      <div class="container hidden-xs visible-lg">
        <!-- Menu Principal -->
        <div class="main-menu wow flipInX" >
          <?php bones_main_nav(); ?>
          <div class="aide-wrapper">
            <a href="#" class="button-aide">Besoin d'Aide ?</a>
          </div>
        </div>

      </div>
      <!-- end .container for header -->
      
    </header>
    